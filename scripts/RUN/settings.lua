local async = require("openmw.async")
local I = require("openmw.interfaces")
local storage = require("openmw.storage")
local MOD_ID = "RUN"
local modSettings = storage.playerSection("Settings" .. MOD_ID)

I.Settings.registerPage {
    key = MOD_ID,
    l10n = MOD_ID,
    name = "name",
    description = "description"
}

I.Settings.registerGroup {
    key = "Settings" .. MOD_ID,
    page = MOD_ID,
    l10n = MOD_ID,
    name = "modSettingsTitle",
    description = "modSettingsDesc",
    permanentStorage = false,
    settings = {
        {
            key = "activateInput",
            name = "activateInput_name",
            description = "activateInput_desc",
            default = "Left Alt",
            renderer = "inputBinding",
            argument = {
                type = "action",
                key = "activateInput"
            }
        },
        {
            key = "doHeadBob",
            name = "doHeadBob_name",
            description = "doHeadBob_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "doBlurShader",
            name = "doBlurShader_name",
            description = "doBlurShader_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "doFovChange",
            name = "doFovChange_name",
            description = "doFovChange_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "maxSpeedBuff",
            name = "maxSpeedBuff_name",
            description = "maxSpeedBuff_desc",
            default = 50,
            min = 1,
            max = 200,
            renderer = "number"
        },
        {
            key = "lowFatigueDrainPerUpdate",
            name = "lowFatigueDrainPerUpdate_name",
            description = "lowFatigueDrainPerUpdate_desc",
            default = 1,
            min = 0,
            max = 50,
            renderer = "number"
        },
        {
            key = "mediumFatigueDrainPerUpdate",
            name = "mediumFatigueDrainPerUpdate_name",
            description = "mediumFatigueDrainPerUpdate_desc",
            default = 2,
            min = 0,
            max = 50,
            renderer = "number"
        },
        {
            key = "highFatigueDrainPerUpdate",
            name = "highFatigueDrainPerUpdate_name",
            description = "highFatigueDrainPerUpdate_desc",
            default = 3,
            min = 0,
            max = 50,
            renderer = "number"
        },
        {
            key = "hbStep",
            name = "hbStep_name",
            description = "hbStep_desc",
            default = 100,
            min = 0,
            max = 200,
            renderer = "number"
        },
        {
            key = "hbHeight",
            name = "hbHeight_name",
            description = "hbHeight_desc",
            default = 12,
            min = -100,
            max = 100,
            renderer = "number"
        },
        {
            key = "hbRoll",
            name = "hbRoll_name",
            description = "hbRoll_desc",
            default = 1.1,
            min = 0,
            max = 10,
            renderer = "number"
        },
        {
            key = "blurIntensity",
            name = "blurIntensity_name",
            description = "blurIntensity_desc",
            default = 0.03,
            min = 0.001,
            max = 0.1,
            renderer = "number"
        },
        {
            key = "cooldown",
            name = "cooldown_name",
            description = "cooldown_desc",
            default = 10,
            min = 1,
            max = 120,
            renderer = "number"
        },
        {
            key = "swimFatigueMod",
            name = "swimFatigueMod_name",
            description = "swimFatigueMod_desc",
            default = 2,
            min = 0,
            max = 100,
            renderer = "number"
        },
        {
            key = "showMsgs",
            name = "showMsgs_name",
            description = "showMsgs_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "zoomInFov",
            name = "zoomInFov_name",
            description = "zoomInFov_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "fovZoomRate",
            name = "fovZoomRate_name",
            description = "fovZoomRate_desc",
            default = 6,
            min = 1,
            max = 10,
            renderer = "number"
        },
        {
            key = "disableWeaponDrawn",
            name = "disableWeaponDrawn_name",
            description = "disableWeaponDrawn_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "disableSwimming",
            name = "disableSwimming_name",
            description = "disableSwimming_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "disableFlying",
            name = "disableFlying_name",
            description = "disableFlying_desc",
            default = false,
            renderer = "checkbox"
        }
    }
}

local activateInput = modSettings:get("activateInput")
local maxSpeedBuff = modSettings:get("maxSpeedBuff")
local lowFatigueDrainPerUpdate = modSettings:get("lowFatigueDrainPerUpdate")
local mediumFatigueDrainPerUpdate = modSettings:get("mediumFatigueDrainPerUpdate")
local highFatigueDrainPerUpdate = modSettings:get("highFatigueDrainPerUpdate")
local hbStep = modSettings:get("hbStep")
local hbHeight = modSettings:get("hbHeight")
local hbRoll = modSettings:get("hbRoll")
local blurIntensity = modSettings:get("blurIntensity")
local cooldown = modSettings:get("cooldown")
local swimFatigueMod = modSettings:get("swimFatigueMod")
local doHeadBob = modSettings:get("doHeadBob")
local doBlurShader = modSettings:get("doBlurShader")
local doFovChange = modSettings:get("doFovChange")
local showMsgs = modSettings:get("showMsgs")
local zoomInFov = modSettings:get("zoomInFov")
local fovZoomRate = modSettings:get("fovZoomRate")
local disableWeaponDrawn = modSettings:get("disableWeaponDrawn")
local disableSwimming = modSettings:get("disableSwimming")
local disableFlying = modSettings:get("disableFlying")

local function updateSettings(_, key)
    if key == "maxSpeedBuff" then
        maxSpeedBuff = modSettings:get("maxSpeedBuff")
    elseif key == "lowFatigueDrainPerUpdate" then
        lowFatigueDrainPerUpdate = modSettings:get("lowFatigueDrainPerUpdate")
    elseif key == "mediumFatigueDrainPerUpdate" then
        mediumFatigueDrainPerUpdate = modSettings:get("mediumFatigueDrainPerUpdate")
    elseif key == "highFatigueDrainPerUpdate" then
        highFatigueDrainPerUpdate = modSettings:get("highFatigueDrainPerUpdate")
    elseif key == "hbStep" then
        hbStep = modSettings:get("hbStep")
    elseif key == "hbHeight" then
        hbHeight = modSettings:get("hbHeight")
    elseif key == "hbRoll" then
        hbRoll = modSettings:get("hbRoll")
    elseif key == "blurIntensity" then
        blurIntensity = modSettings:get("blurIntensity")
    elseif key == "cooldown" then
        cooldown = modSettings:get("cooldown")
    elseif key == "swimFatigueMod" then
        swimFatigueMod = modSettings:get("swimFatigueMod")
    elseif key == "doHeadBob" then
        doHeadBob = modSettings:get("doHeadBob")
    elseif key == "doBlurShader" then
        doBlurShader = modSettings:get("doBlurShader")
    elseif key == "doFovChange" then
        doFovChange = modSettings:get("doFovChange")
    elseif key == "showMsgs" then
        showMsgs = modSettings:get("showMsgs")
    elseif key == "activateInput" then
        activateInput = modSettings:get("activateInput")
    elseif key == "zoomInFov" then
        zoomInFov = modSettings:get("zoomInFov")
    elseif key == "fovZoomRate" then
        fovZoomRate = modSettings:get("fovZoomRate")
    elseif key == "disableWeaponDrawn" then
        disableWeaponDrawn = modSettings:get("disableWeaponDrawn")
    elseif key == "disableSwimming" then
        disableSwimming = modSettings:get("disableSwimming")
    elseif key == "disableFlying" then
        disableFlying = modSettings:get("disableFlying")
    end
end
modSettings:subscribe(async:callback(updateSettings))

return {
    MOD_ID = MOD_ID,
    cooldown = function() return cooldown end,
    swimFatigueMod = function() return swimFatigueMod end,
    doHeadBob = function() return doHeadBob end,
    doBlurShader = function() return doBlurShader end,
    doFovChange = function() return doFovChange end,
    lowFatigueDrainPerUpdate = function() return lowFatigueDrainPerUpdate end,
    mediumFatigueDrainPerUpdate = function() return mediumFatigueDrainPerUpdate end,
    highFatigueDrainPerUpdate = function() return highFatigueDrainPerUpdate end,
    hbStep = function() return hbStep end,
    hbHeight = function() return hbHeight end,
    hbRoll = function() return hbRoll end,
    blurIntensity = function() return blurIntensity end,
    maxSpeedBuff = function() return maxSpeedBuff end,
    showMsgs = function() return showMsgs end,
    activateInput = function() return activateInput end,
    zoomInFov = function() return zoomInFov end,
    fovZoomRate = function() return fovZoomRate end,
    disableWeaponDrawn = function() return disableWeaponDrawn end,
    disableSwimming = function() return disableSwimming end,
    disableFlying = function() return disableFlying end
}
