local core = require("openmw.core")
if core.API_REVISION < 69 then
	error("RUN! requires OpenMW 0.49 or newer!")
end
local async = require("openmw.async")
local camera = require("openmw.camera")
local debug = require("openmw.debug")
local input = require("openmw.input")
local postprocessing = require("openmw.postprocessing")
local self = require("openmw.self")
local storage = require("openmw.storage")
local ui = require("openmw.ui")
local aux_util = require("openmw_aux.util")
local settings = require("scripts.RUN.settings")

local blur = postprocessing.load("radial-blur")
blur:setFloat("uRadius", settings.blurIntensity())

local MOD_ID = settings.MOD_ID
local L = core.l10n(MOD_ID)
local interfaceVersion = 1
local scriptVersion = 1
local modSettings = storage.playerSection("Settings" .. MOD_ID)

local counterMin = -1.5
local counterMax = 2
local counter = counterMin
local active = false
local totalDt = 0
local headBobbing = storage.playerSection("SettingsOMWCameraHeadBobbing")
local hbSettings
local modifiedSpeed
local cooldown = false
local chargenDone = false

local function updateBlurIntensity(_, key)
    if key == "blurIntensity" then
        blur:setFloat("uRadius", settings.blurIntensity())
    end
end
modSettings:subscribe(async:callback(updateBlurIntensity))

input.registerAction {
    key = "activateInput",
    type = input.ACTION_TYPE.Boolean,
    l10n = "RUN",
    defaultValue = false
}

local function msg(...)
    if settings.showMsgs()  then
        ui.showMessage(..., {showInDialogue = false})
    end
end

local function fatigueDrainPerUpdate()
    if debug.isGodMode() then return 0 end
    local acrobatics = self.type.stats.skills.acrobatics(self).modified
    local athletics = self.type.stats.skills.athletics(self).modified
    local chance = (200 - (acrobatics + athletics)) / 2
    local drainAmt = 0
    if chance < 34 then
        drainAmt = settings.lowFatigueDrainPerUpdate()
    elseif chance >= 34 and chance < 66 then
        drainAmt = settings.mediumFatigueDrainPerUpdate()
    elseif chance >= 67 then
        drainAmt = settings.highFatigueDrainPerUpdate()
    end
    if self.type.isSwimming(self) then
        drainAmt = drainAmt * settings.swimFatigueMod()
    end
    return math.min(drainAmt, self.type.stats.dynamic.fatigue(self).current)
end

local function speedBuff()
    if debug.isGodMode() then return settings.maxSpeedBuff() end
    local acrobatics = self.type.stats.skills.acrobatics(self).modified
    local athletics = self.type.stats.skills.athletics(self).modified
    return math.min(settings.maxSpeedBuff(), math.floor((acrobatics + athletics) / 5))
end

local function handleBlur()
    if active and not blur:isEnabled() and settings.doBlurShader() then
        blur:enable()
    elseif not active and blur:isEnabled() then
        blur:disable()
    end
end

local function modFatigue(data)
    totalDt = totalDt + data.dt
    if data.keybindPressed and totalDt >= .5 then
        self.type.stats.dynamic.fatigue(self).current = self.type.stats.dynamic.fatigue(self).current - fatigueDrainPerUpdate()
        totalDt = 0
    end
end

local function modFOV(data)
    if data.keybindPressed then
        counter = math.min(counterMax, counter + data.dt * settings.fovZoomRate())
    else
        counter = math.max(counterMin, counter - data.dt * settings.fovZoomRate())
    end
    local effect = ((math.max(0.1, math.exp(math.min(1, counter)-1)) - 0.1) / 0.9) / 3
    local newFOV = camera.getBaseFieldOfView()
    if settings.zoomInFov() then
        newFOV = newFOV * (1 - 0.5 * effect)
    else
        newFOV = newFOV * (1 + 0.5 * effect)
    end
    if settings.doFovChange() then
        camera.setFieldOfView(newFOV)
    else
        camera.setFieldOfView(camera.getBaseFieldOfView())
    end
end

local function modHeadBobbing()
    if active and hbSettings == nil and settings.doHeadBob() then
        hbSettings = {}
        hbSettings.step = headBobbing:get("enabled")
        hbSettings.height = headBobbing:get("height")
        hbSettings.roll = headBobbing:get("roll")
        headBobbing:set("enabled", settings.hbStep())
        headBobbing:set("height", settings.hbHeight())
        headBobbing:set("roll", settings.hbRoll())
    elseif not active and hbSettings ~= nil then
        headBobbing:set("enabled", hbSettings.step)
        headBobbing:set("height", hbSettings.height)
        headBobbing:set("roll", hbSettings.roll)
        hbSettings = nil
    end
end

local function modSpeed(keybindPressed)
    --TODO: Scale up speed instead of instant, full buff https://gitlab.com/modding-openmw/RUN/-/issues/1
    if keybindPressed and modifiedSpeed == nil then
        modifiedSpeed = speedBuff()
        self.type.stats.attributes.speed(self).modifier = self.type.stats.attributes.speed(self).modifier + modifiedSpeed
    elseif not keybindPressed and modifiedSpeed ~= nil and not active then
        self.type.stats.attributes.speed(self).modifier = self.type.stats.attributes.speed(self).modifier - modifiedSpeed
        modifiedSpeed = nil
    end
end

local function onLoad(data)
    active = data.active
    chargenDone = data.chargenDone
    cooldown = data.cooldown
    hbSettings = data.hbSettings
    modifiedSpeed = data.modifiedSpeed
end

local function onSave()
    return {
        active = active,
        chargenDone = chargenDone,
        cooldown = cooldown,
        hbSettings = hbSettings,
        modifiedSpeed = modifiedSpeed,
        version = scriptVersion
    }
end

local chargenDoneCb = async:registerTimerCallback(
    "chargenDone", function() msg(L("plzSetKey")) end
)

local cooldownCb = async:registerTimerCallback(
    "run_cooldownCallback",
    function()
        msg(L("sprintAgain"))
        cooldown = false
    end
)

local function onUpdate(deltaTime)
    if not chargenDone and input.getControlSwitch(input.CONTROL_SWITCH.ViewMode) then
        chargenDone = true
        async:newSimulationTimer(5, chargenDoneCb)
    elseif not chargenDone then
        return
    end
    if settings.disableSwimming() and self.type.isSwimming(self) then return end
    if settings.disableFlying() and not self.type.isOnGround(self) then return end
    local prereqs = self.controls.movement == 1
        -- Don't sprint if attack is being held down
        and self.controls.use == 0
        -- Don't sprint if we're walking instead of running
        and self.controls.run
        -- Don't sprint if we're sneaking
        and not self.controls.sneak
    if settings.disableWeaponDrawn() then
        prereqs = prereqs and self.type.getStance(self) < 1
    end
    local keybindPressed = input.getBooleanActionValue("activateInput") and prereqs
    -- Override if we're in cooldown
    if cooldown then keybindPressed = false end
    if not active and keybindPressed then
        active = true
    elseif active and not keybindPressed and camera.getFieldOfView() == camera.getBaseFieldOfView() then
        active = false
    end

    -- Work
    handleBlur()
    modFOV({dt = deltaTime, keybindPressed = keybindPressed})
    modHeadBobbing()
    modFatigue({dt = deltaTime, keybindPressed = keybindPressed})
    modSpeed(keybindPressed)

    -- Remember to properly pass out!
    if active
        and not cooldown
        and self.type.stats.dynamic.fatigue(self).current == 0 then
        msg(L("pastOut")) -- https://www.youtube.com/watch?v=8DdPvon5jIs
        self.type.stats.dynamic.fatigue(self).current = self.type.stats.dynamic.fatigue(self).current - 1
        active = false
        if blur:isEnabled() then blur:disable() end
        cooldown = true
        if hbSettings then
            headBobbing:set("enabled", hbSettings.step)
            headBobbing:set("height", hbSettings.height)
            headBobbing:set("roll", hbSettings.roll)
            hbSettings = nil
        end
        async:newGameTimer(settings.cooldown(), cooldownCb)
    end
end

return {
    engineHandlers = {
        onLoad = onLoad,
        onSave = onSave,
        onUpdate = onUpdate
    },
    interfaceName = MOD_ID,
    interface = {
        Active = function() return active end,
        FatigueDrain = function() return fatigueDrainPerUpdate() end,
        HbSettings = function() return aux_util.deepToString(hbSettings, 4) end,
        SpeedBuff = function() return speedBuff() end,
        version = interfaceVersion
    }
}
