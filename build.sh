#!/bin/sh
set -eu

#
# This script packages the project into a zip file.
#

file_name=RUN.zip

cat > RUN-metadata.toml <<EOF
[package]
name = "RUN!"
description = "A sprinting mod for OpenMW 0.49 or newer. Highly configurable."
homepage = "https://modding-openmw.gitlab.io/run/"
authors = [
	"johnnyhostile",
]
version = "$(git describe --tags --always)"
EOF

zip --must-match --recurse-paths ${file_name} scripts shaders CHANGELOG.md LICENSE README.org RUN.omwscripts RUN-metadata.toml l10n
sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
