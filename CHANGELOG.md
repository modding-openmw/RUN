## RUN Changelog

#### Version 1.2

* Fixed a problem with incorrect handling of non-constant effect speed fortifications from items/spells ([#4](https://gitlab.com/modding-openmw/RUN/-/issues/4))
* Fixed a problem that would have occurred if the player raised athletics or acrobatics while sprinting ([#4](https://gitlab.com/modding-openmw/RUN/-/issues/4))

[Download Link](https://gitlab.com/modding-openmw/RUN/-/packages/36197838)

#### Version 1.1

* The FOV shift is now faster and configurable
* Fixed a problem that caused FOV stuttering when other mods alter FOV (even if they're patched to work with this one)
* Fixed a problem where the sprint buff would negate existing Speed fortifications

[Download Link](https://gitlab.com/modding-openmw/RUN/-/packages/35094892)

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/RUN/-/packages/34096472)
