#+TITLE: RUN!
#+options: author:nil toc:nil

A sprinting mod for OpenMW 0.49 or newer. Highly configurable.

*** Installation

*OpenMW 0.49 or newer is required!*

**** With umo

1. Click the "Install with umo" button at the top of [[https://modding-openmw.gitlab.io/run/][this URL]]
2. Run =umo cache sync custom=, then =umo install custom=

Adjust =custom= above if you're using a named custom list.

**** Manual

1. Click the "Manual Download" button at the top of [[https://modding-openmw.gitlab.io/RUN/][this URL]]
2. Extract the zip to a location of your choosing, examples below:

#+begin_src
# Windows
C:\games\OpenMWMods\Gameplay\RUN

# Linux
/home/username/games/OpenMWMods/Gameplay/RUN

# macOS
/Users/username/games/OpenMWMods/Gameplay/RUN
#+end_src

**** Enable

1. Add the appropriate data path to your ~opemw.cfg~ file (e.g. ~data="C:\games\OpenMWMods\Gameplay\RUN"~)
2. Add ~RUN.omwscripts~ to your load order in ~openmw.cfg~ or enable it via OpenMW-Launcher

*** Recommended Mods

Some kind of mod that rebalances fatigue regeneration (for example: [[https://www.nexusmods.com/morrowind/mods/52981][Fatigue and Speed and Carryweight Rebalance (OpenMW)]]) is strongly recommended!

*** Settings

- *Activate Sprint Button Or Key* (Default: *Unset*)
- *Use Headbob Effect When Sprinting* (Default: *Yes*)
- *Use Blur Shader When Sprinting* (Default: *Yes*)
- *Alter FOV When Sprinting* (Default: *Yes*)
- *Maximum Speed Buff* (Default: *50*)
- *Low Fatigue Drain Amount* (Default: *1*)
- *Medium Fatigue Drain Amount* (Default: *2*)
- *High Fatigue Drain Amount* (Default: *3*)
- *Head Bobbing Step* (Default: *100*)
- *Head Bobbing Height* (Default: *12*)
- *Head Bobbing Roll* (Default: *1.1*)
- *Blur Shader Intensity* (Default: *0.03*)
- *Cooldown Time* (Default: *10*)
- *Swimming Fatigue Drain Modifier* (Default: *2*)
- *Show Messages* (Default: *Yes*)
- *Zoom In For FOV Change* (Default: *No*)
- *FOV Zoom Rate* (Default: *6*)
- *Disable When Weapon Or Spell Is Readied* (Default: *No*)
- *Disable When Swimming* (Default: *No*)
- *Disable When Flying* (Default: *No*)

*** Compatibility

This mod will conflict with any other mod that tries to adjust FOV un the ~onUpdate~ engine handler, though it is possible to update such mods to play nicely with this one.

Please see the [[#player-lua-interface][Player Lua Interface]] section below for more information about how to make these kinds of mods play nicely with this one.

*** Credits

Author: *johnnyhostile*

Blur shader: *Epoch*

*Special Thanks*:

- *Petr Mikheev* for creating the FOV changing code that this mod's code is based on
- *The OpenMW team, including every contributor* for making OpenMW and OpenMW-CS
- *The Modding-OpenMW.com team* for being amazing
- *All the users in the ~modding-openmw-dot-com~ Discord channel on the OpenMW server as well as the showcase thread for this mod* for their dilligent testing and feedback ❤️
- *Bethesda* for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

*** Localization

- EN: johnnyhostile

*** Web

- [[https://modding-openmw.gitlab.io/RUN/][Project Home]]
- [[https://modding-openmw.gitlab.io/update-center/][Update Center]]
- [[https://discord.com/channels/260439894298460160/1327420529958846476][Discord thread on the OpenMW server]]

*** Player Lua Interface

Use this from a player script or after doing ~luap~ in the in-game console:

- ~Active()~

Print ~true~ or ~false~ based on if the player is sprinting or not.

#+begin_src lua
I.RUN.Active()
#+end_src

Other mods that use the same pattern for updating FOV can use this to play nicely with this one:

#+begin_src lua
if I.RUN and not I.RUN.Active() then
  -- Do FOV changes and anything else here
end
#+end_src

- ~FatigueDrain()~

Returns the number of fatigue points that are deducted per half second while sprinting.

#+begin_src lua
I.RUN.FatigueDrain()
#+end_src

Look at the ~fatigueDrainPerUpdate()~ function in ~scripts/RUN/player.lua~ for more information about how this is calculated.

- ~HbSettings()~

Returns a table of head bobbing settings that the mod temporarily remembers when it sets its own values.

#+begin_src lua
I.RUN.HbSettings()
#+end_src

- ~SpeedBuff()~

Returns the number of points the Speed attribute is buffed while sprinting.

#+begin_src lua
I.RUN.SpeedBuff()
#+end_src

The code that determines the speed buff is roughly:

#+begin_src lua
math.min(maxSpeedBuff, math.floor((acrobatics + athletics) / 5))
#+end_src

Used skill values do include modifiers!

*** Report A Problem

- [[https://gitlab.com/modding-openmw/RUN/-/issues][Open an issue on GitLab]]
- Email ~run-mod@modding-openmw.com~
- Contact the author on Discord: ~@johnnyhostile~
- Contact the author on Libera.chat IRC: ~johnnyhostile~
